//
//  ContentView.swift
//  SwiftUi-1
//
//  Created by Techwens on 27/03/24.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack() {
            
            Text("Hello, world! this is swift ui text example for text attribute".uppercased().capitalized)
                .font(.title3)
//                .fontWeight(.semibold)
//                .bold()
//                .underline()
//                .underline(pattern: .dashDot , color:.red)
//                .strikethrough(true,color:.green)
//                .italic()
            
//                .font(.system(size:24,weight: .bold,design: .rounded))
//                .baselineOffset(-120)  ---> for line spacking
//                .kerning(10)  ----> for word spacing
//                .multilineTextAlignment(.center)  ---> for centering text
            
//                .foregroundColor(.red)
                .frame(width: 200,height: 200, alignment: .leading)
                .minimumScaleFactor(0.1)
            
              
                
               
        }
        
        
        
        
        
    }
}

#Preview {
    ContentView()
}
