//
//  SwiftUi_1App.swift
//  SwiftUi-1
//
//  Created by Techwens on 27/03/24.
//

import SwiftUI

@main
struct SwiftUi_1App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
